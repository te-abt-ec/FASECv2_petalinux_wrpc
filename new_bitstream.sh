#!/bin/bash -x

# bitstream copy and .BIT generation
fm=../../FASEC_prototype/firmware/system_design_wrapper.bit
ls -l $fm
cp $fm ./images/linux/system_design_wrapper.bit
cp $fm ./subsystems/linux/hw-description/system_design_wrapper.bit
petalinux-package --boot --fsbl images/linux/zynq_fsbl.elf --fpga images/linux/system_design_wrapper.bit --u-boot images/linux/u-boot.elf --force

sudo cp BOOT.BIN /run/media/pieter/vmcc7tftp/nfsroot/home/root/
