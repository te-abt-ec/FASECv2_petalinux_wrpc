/* fasec_hwtest.c
 * Based on petalinux module template, modified for testing FIDS hardware
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/i2c.h>

#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_platform.h>

/* Standard module information, edit as appropriate */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("pvantrap");
MODULE_DESCRIPTION("fasec_hwtest - simple module that allows FIDS hardware testing with interrupts");

#define DRIVER_NAME "fasec_hwtest"
#define FASECBASE 0x00
#define FMC1BASE  0x08
#define FMC2BASE  0x4c
#define INCOMP    0x00
#define INCOMPEX  0x04
#define OUTEX	0x05
#define OUT 	0x06

#define PATCH_I2C_ADAPTER	0
#define PATCH_I2C_IC1_ADDR	0x77
#define PATCH_I2C_IC2_ADDR	0x75
#define PATCH_I2C_IC3_ADDR	0x74
#define TCA9539_CONF_P0		0x06
#define TCA9539_CONF_P1		0x07
#define TCA9539_OUT_P0		0x02
#define TCA9539_OUT_P1		0x03
#define MAXARRAY		16
/* bitvector positions for ic3, leds1-16 */
static int ic3_led_positions[MAXARRAY] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 11, 12, 13, 14, 15, 16 };
#define FMC1_LED1	1
#define FMC1_LED1_OUT	11
/* bitvector positions for ic2, leds17-18,29-30,37-40 */
static int ic2_led_positions[MAXARRAY] = { 0, 0, 0, 0, 0, 0, 0, 0, 37, 38, 39, 40, 29, 30, 17, 18 };
/* bitvector positions for ic1, leds19-28,31-36 */
static int ic1_led_positions[MAXARRAY] = { 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 31, 32, 33, 34, 35, 36};
#define FMC2_LED1	19
#define FMC2_LED1_OUT	29

static struct i2c_board_info patch_info[] = {
	{
		I2C_BOARD_INFO("patch_ic1",PATCH_I2C_IC1_ADDR),
	},
	{
		I2C_BOARD_INFO("patch_ic2",PATCH_I2C_IC2_ADDR),
	},
	{
		I2C_BOARD_INFO("patch_ic3",PATCH_I2C_IC3_ADDR),
	},
};

/* Simple example of how to receive command line parameters to your module.
   Delete if you don't need them */
unsigned myint = 0xdeadbeef;
char *mystr = "default";

module_param(myint, int, S_IRUGO);
module_param(mystr, charp, S_IRUGO);

struct fasec_hwtest_local {
	int irq;
	unsigned long mem_start;
	unsigned long mem_end;
	void __iomem *base_addr;
	struct i2c_client *i2c_ic1;
	struct i2c_client *i2c_ic2;
	struct i2c_client *i2c_ic3;
	u32 fmc1_incomp, fmc2_incomp, fmc1_out, fmc2_out;
	u32 fmc1_incompex, fmc2_incompex, fmc1_outex, fmc2_outex;
};

static void fasec_flash_leds(struct fasec_hwtest_local *lpp)
{
	/* val is negated because active-low connected LEDs
	 * always status | extended to visualise short pulses as well
	 * FIXME: lock_mutex on incompex data
	*/
	
	int res, i, ai;
	u16 val, led_on, mask;
	struct i2c_client *ic;
	const int IN_MAXBITS = 10;
	const int OUT_MAXBITS = 8;
	const int OUT2_MAXBITS = 2;
	
	/* ic1 - FMC2 IO */
	ic = lpp->i2c_ic1;
	val = 0x0;
	for (i=0; i<IN_MAXBITS; i++){
		// masks looks at both channels per input, which has also only one LED; 1 bit high suffices
		mask = 1U<<(i*2) | 1U<<(i*2+1);
		led_on = ((lpp->fmc2_incompex | lpp->fmc2_incomp) & mask)?1:0;
		ai = 0;
		while (ai<MAXARRAY && ic1_led_positions[ai] != i+FMC2_LED1) ai++;
		val = val | led_on<<ai;
	}
	for (i=OUT2_MAXBITS; i<OUT_MAXBITS; i++){ //FMC2, ignore first 2 LEDs
		mask = 1U<<i;
		led_on = ((lpp->fmc2_outex | lpp->fmc2_out) & mask)?1:0;
		ai = 0;
		while (ai<MAXARRAY && ic1_led_positions[ai] != i+FMC2_LED1_OUT) ai++;
		val = val | led_on<<ai;
	}
	res = i2c_smbus_write_word_data(ic, TCA9539_OUT_P0, ~val);
	if (res<0){
		pr_err("i2c_write for %d failed\n", ic->addr);
	}

	/* ic2 - outs from FMC2 & FMC1
	 * overly complex cause of how LEDs are differently connected and ordered per FMC on the patch..
	 */
	ic = lpp->i2c_ic2;
	val = 0x0;
	for (i=0; i<OUT2_MAXBITS; i++){	//FMC2, first 2 LEDs
		mask = 1U<<i;
		led_on = ((lpp->fmc2_outex | lpp->fmc2_out) & mask)?1:0;
		ai = 0;
		while (ai<MAXARRAY && ic2_led_positions[ai] != i+FMC2_LED1_OUT) ai++;
		val = val | led_on<<ai;
	}
	for (i=OUT_MAXBITS-OUT2_MAXBITS; i<OUT_MAXBITS; i++){	//FMC1, last 2 LEDs
		mask = 1U<<i;		
		led_on = ((lpp->fmc1_outex | lpp->fmc1_out) & mask)?1:0;
		ai = 0;
		while (ai<MAXARRAY && ic2_led_positions[ai] != i+FMC1_LED1_OUT) ai++;
		val = val | led_on<<ai;
	}
	res = i2c_smbus_write_word_data(ic, TCA9539_OUT_P0, ~val);
	if (res<0){
		pr_err("i2c_write for %d failed\n", ic->addr);
	}
	
	/* ic3 - FMC1 IO */
	ic = lpp->i2c_ic3;
	val = 0x0;
	for (i=0; i<IN_MAXBITS; i++){
		mask = 1U<<(i*2) | 1U<<(i*2+1);		
		led_on = ((lpp->fmc1_incompex | lpp->fmc1_incomp) & mask)?1:0;
		ai = 0;
		while (ai<MAXARRAY && (ic3_led_positions[ai] != i+FMC1_LED1)) ai++;
		val = val | led_on<<ai;
	}
	for (i=0; i<OUT_MAXBITS-OUT2_MAXBITS; i++){ //FMC1, ignore last 2 LEDs
		mask = 1U<<i;
		led_on = ((lpp->fmc1_outex | lpp->fmc1_out) & mask)?1:0;
		ai = 0;
		while (ai<MAXARRAY && (ic3_led_positions[ai] != i+FMC1_LED1_OUT)) ai++;
		val = val | led_on<<ai;
	}
	res = i2c_smbus_write_word_data(ic, TCA9539_OUT_P0, ~val);
	if (res<0){
		pr_err("i2c_write for %d failed\n", ic->addr);
	}
}

static irqreturn_t fasec_hwtest_irq_fn(int irq, void *lp)
{
	/* this function, called by isr, executes the slow i2c comm */
	struct fasec_hwtest_local *lpp = lp;
	fasec_flash_leds(lpp);
	return IRQ_HANDLED;

}

static irqreturn_t fasec_hwtest_irq(int irq, void *lp)
{
	struct fasec_hwtest_local *lpp = lp;

	u32 addr1, addr2, addr3, addr4;
	addr1 = (u32)lpp->base_addr+((FMC1BASE+INCOMP)<<2);
	addr2 = (u32)lpp->base_addr+((FMC1BASE+INCOMPEX)<<2);
	addr3 = (u32)lpp->base_addr+((FMC2BASE+INCOMP)<<2);
	addr4 = (u32)lpp->base_addr+((FMC2BASE+INCOMPEX)<<2);

	lpp->fmc1_incompex = ioread32((void*)addr2);
	lpp->fmc1_outex = ioread32((void*)(lpp->base_addr+((FMC1BASE+OUTEX)<<2)));
	lpp->fmc1_incomp = ioread32((void*)(lpp->base_addr+((FMC1BASE+INCOMP)<<2)));
	lpp->fmc1_out = ioread32((void*)(lpp->base_addr+((FMC1BASE+OUT)<<2)));	

	lpp->fmc2_incompex = ioread32((void*)addr4);
	lpp->fmc2_outex = ioread32((void*)(lpp->base_addr+((FMC2BASE+OUTEX)<<2)));	
	lpp->fmc2_incomp = ioread32((void*)(lpp->base_addr+((FMC2BASE+INCOMP)<<2)));
	lpp->fmc2_out = ioread32((void*)(lpp->base_addr+((FMC2BASE+OUT)<<2)));	
	
	pr_debug("FMC1 inputs / inputs extended: 0x%08x / 0x%08x : 0x%08x / 0x%08x\n",
	       (unsigned int __force) addr1,
	       (unsigned int __force) addr2,	       
	       (unsigned int __force) ioread32((void*)addr1),
	       (unsigned int __force) ioread32((void*)addr2));

	return IRQ_WAKE_THREAD;
}

static int fasec_hwtest_probe(struct platform_device *pdev)
{
	struct resource *r_irq; /* Interrupt resources */
	struct resource *r_mem; /* IO mem resources */
	struct device *dev = &pdev->dev;
	struct fasec_hwtest_local *lp = NULL;
	struct i2c_adapter *patch_adap;
	int res;
	u16 val = 0x0;

	int rc = 0;
	
	dev_info(dev, "Device Tree Probing\n");

	/* Get iospace for the device */
	r_mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!r_mem) {
		dev_err(dev, "invalid address\n");
		return -ENODEV;
	}
	
	lp = (struct fasec_hwtest_local *) kmalloc(sizeof(struct fasec_hwtest_local), GFP_KERNEL);
	if (!lp) {
		dev_err(dev, "Cound not allocate fasec_hwtest device\n");
		return -ENOMEM;
	}
	
	dev_set_drvdata(dev, lp);
	
	lp->mem_start = r_mem->start;
	lp->mem_end = r_mem->end;

	/* devm ioremap */
	lp->base_addr = devm_ioremap_resource(dev, r_mem);
	if (IS_ERR(lp->base_addr))
		return PTR_ERR(lp->base_addr);

	/* register i2c mux devices, ENSURE they are not in the device-tree!
	 * FIXME: put in separate function
	 */
	patch_adap = i2c_get_adapter(PATCH_I2C_ADAPTER);
	if (!patch_adap){
		pr_err("%s: i2c_get_adapter(%d) failed\n", __func__,
		       PATCH_I2C_ADAPTER);
		return -EINVAL;
	}
	/* ic1 */
	lp->i2c_ic1 = i2c_new_device(patch_adap, &patch_info[0]);
	if (!lp->i2c_ic1){
		pr_err("%s: i2c_new_device(%d) failed\n", __func__,
		       PATCH_I2C_IC1_ADDR);
		i2c_put_adapter(patch_adap);
		return -EINVAL;
	}
	val = 0x0;
	res = i2c_smbus_write_word_data(lp->i2c_ic1, TCA9539_CONF_P0, val);
	if (res<0){
		pr_err("i2c_write for %d failed\n", lp->i2c_ic1->addr);
	}
	/* ic2 */
	lp->i2c_ic2 = i2c_new_device(patch_adap, &patch_info[1]);
	if (!lp->i2c_ic2){
		pr_err("%s: i2c_new_device(%d) failed\n", __func__,
		       PATCH_I2C_IC2_ADDR);
		i2c_put_adapter(patch_adap);
		return -EINVAL;
	}
	val = 0x00ff;
	res = i2c_smbus_write_word_data(lp->i2c_ic2, TCA9539_CONF_P0, val);
	if (res<0){
		pr_err("i2c_write for %d failed\n", lp->i2c_ic2->addr);
	}
	/* ic3 */
	lp->i2c_ic3 = i2c_new_device(patch_adap, &patch_info[2]);
	if (!lp->i2c_ic3){
		pr_err("%s: i2c_new_device(%d) failed\n", __func__,
		       PATCH_I2C_IC3_ADDR);
		i2c_put_adapter(patch_adap);
		return -EINVAL;
	}
	val = 0x0;
	res = i2c_smbus_write_word_data(lp->i2c_ic3, TCA9539_CONF_P0, val);
	if (res<0){
		pr_err("i2c_write for %d failed\n", lp->i2c_ic3->addr);
	}

	/* devm irq, AFTER i2c_client creation cause otherwise irq could happen before _probe returns */
	r_irq = platform_get_resource(pdev, IORESOURCE_IRQ, 0);
	if (!r_irq) {
		dev_info(dev, "no IRQ found\n");
		dev_info(dev, "fasec_hwtest at 0x%08x mapped to 0x%08x\n",
			(unsigned int __force)lp->mem_start,
			(unsigned int __force)lp->base_addr);
	} else {
		lp->irq = r_irq->start;
		rc = devm_request_threaded_irq(dev, lp->irq, fasec_hwtest_irq, fasec_hwtest_irq_fn, 0, DRIVER_NAME, lp);
		if (rc) {
			dev_err(dev, "testmodule: Could not allocate interrupt.\n");
			return rc;
		}

		dev_info(dev,"fasec_hwtest at 0x%08x mapped to 0x%08x, irq=%d\n",
			 (unsigned int __force)lp->mem_start,
			 (unsigned int __force)lp->base_addr,
			 lp->irq);
	}

	return 0;
}

static int fasec_hwtest_remove(struct platform_device *pdev)
{
	int res;
	struct device *dev = &pdev->dev;
	struct fasec_hwtest_local *lp = dev_get_drvdata(dev);

	/* switch off all LEDs */
	res = i2c_smbus_write_word_data(lp->i2c_ic1, TCA9539_OUT_P0, 0xffff);
	if (res<0)
		pr_err("i2c_write failed during _remove\n");
	res = i2c_smbus_write_word_data(lp->i2c_ic2, TCA9539_OUT_P0, 0xffff);
	if (res<0)
		pr_err("i2c_write failed during _remove\n");
	res = i2c_smbus_write_word_data(lp->i2c_ic3, TCA9539_OUT_P0, 0xffff);
	if (res<0)
		pr_err("i2c_write failed during _remove\n");
	
	i2c_unregister_device(lp->i2c_ic1);
	i2c_unregister_device(lp->i2c_ic2);
	i2c_unregister_device(lp->i2c_ic3);
	kfree(lp);
	dev_set_drvdata(dev, NULL);
	
	return 0;
}

#ifdef CONFIG_OF
static struct of_device_id fasec_hwtest_of_match[] = {
	{ .compatible = "cern,fasec_hwtest", },
	{ /* end of list */ },
};
MODULE_DEVICE_TABLE(of, fasec_hwtest_of_match);
#else
# define fasec_hwtest_of_match
#endif


static struct platform_driver fasec_hwtest_driver = {
	.driver = {
		.name = DRIVER_NAME,
		.owner = THIS_MODULE,
		.of_match_table	= fasec_hwtest_of_match,
	},
	.probe		= fasec_hwtest_probe,
	.remove		= fasec_hwtest_remove,
};

static int __init fasec_hwtest_init(void)
{
	printk("<1>Hello module world.\n");
	printk("<1>Module parameters were (0x%08x) and \"%s\"\n", myint,
	       mystr);

	return platform_driver_register(&fasec_hwtest_driver);
}


static void __exit fasec_hwtest_exit(void)
{
	platform_driver_unregister(&fasec_hwtest_driver);
	printk(KERN_ALERT "FASEC_hwtest module unloaded\n");
}

module_init(fasec_hwtest_init);
module_exit(fasec_hwtest_exit);

