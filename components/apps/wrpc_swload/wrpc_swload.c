/*
 * WRPC LM32 software loader for FASEC
 * (c) CERN, Pieter Van Trappen
 * based on ohwr.org SPEC loader
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <arpa/inet.h>

int fasec_writel(uint32_t addr, uint32_t val)
{
	int fd;
	void *ptr;
	unsigned page_addr, page_offset;
	unsigned page_size=sysconf(_SC_PAGESIZE);

	fd=open("/dev/mem",O_RDWR);
	if(fd<1)
		return -1;

	page_addr=(addr & ~(page_size-1));
	page_offset=addr-page_addr;

	ptr=mmap(NULL,page_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,(addr & ~(page_size-1)));
	if((int)ptr==-1)
		return -2;

	*((unsigned *)(ptr+page_offset))=val;
	close(fd);
	return 0;
}

uint32_t fasec_readl(uint32_t addr)
{
	int fd;
	void *ptr;
	unsigned page_addr, page_offset;
	unsigned page_size=sysconf(_SC_PAGESIZE);
	unsigned ret;

	fd=open("/dev/mem",O_RDONLY);
	if(fd<1)
		return -1;

	page_addr=(addr & ~(page_size-1));
	page_offset=addr-page_addr;

	ptr=mmap(NULL,page_size,PROT_READ,MAP_SHARED,fd,(addr & ~(page_size-1)));
	if((int)ptr==-1)
		return -2;

	ret=*(unsigned *)(ptr+page_offset);
	close(fd);
	return (uint32_t)ret;
}

static char *load_binary_file(const char *filename, size_t *size)
{
	int i;
	struct stat stbuf;
	char *buf;
	FILE *f;

	f = fopen(filename, "r");
	if (!f) {
		fprintf(stderr, "%s: %s\n", filename, strerror(errno));
		return NULL;
	}
	if (fstat(fileno(f), &stbuf) < 0) {
		fprintf(stderr, "%s: %s\n", filename, strerror(errno));
		fclose(f);
		return NULL;
	}

	if (!S_ISREG(stbuf.st_mode)) {
		fprintf(stderr, "%s: not a regular file\n", filename);
		fclose(f);
		return NULL;
	}

	buf = malloc(stbuf.st_size);
	if (!buf) {
		fprintf(stderr, "loading %s: %s\n", filename, strerror(errno));
		fclose(f);
		return NULL;
	}

	i = fread(buf, 1, stbuf.st_size, f);
	fclose(f);
	if (i < 0) {
		fprintf(stderr, "reading %s: %s\n", filename, strerror(errno));
		free(buf);
		return NULL;
	}
	if (i != stbuf.st_size) {
		fprintf(stderr, "%s: short read\n", filename);
		free(buf);
		return NULL;
	}

	*size = stbuf.st_size;
	return buf;
}

int fasec_load_lm32(const char *filename, uint32_t base_addr)
{
	char *buf;
	uint32_t *ibuf;
	size_t size;
	int i, ret;

	buf = load_binary_file(filename, &size);
	if(!buf)
		return -1;

	fasec_writel(base_addr + 0x20400, 0x1deadbee);
	while ( ! (fasec_readl(base_addr + 0x20400) & (1<<28)) );

	ibuf = (uint32_t *) buf;
	for (i = 0; i < (size + 3) / 4; i++){
		ret = fasec_writel(base_addr + i*4, htonl(ibuf[i]));
		if (ret<0){
			printf("writing fault %d at word %d\/%d\n", ret, i, size/4);
			return -1;
		}
	}

	printf("writing done\n");
	
	sync();

	for (i = 0; i < (size + 3) / 4; i++) {
		uint32_t r = fasec_readl(base_addr + i*4);
		if (r != htonl(ibuf[i]))
		{
			fprintf(stderr, "programming error at %x "
				"(expected %08x, found %08x)\n", i*4,
				htonl(ibuf[i]), r);
			return -1;
		}
	}

	sync();
	printf("readback done, starting LM32..\n");
	
	fasec_writel(base_addr + 0x20400, 0x0deadbee);
	return 0;
}

int main(int argc, char *argv[])
{
	int c;
	uint32_t wrpc_base = 0x80000000;

	while ((c = getopt (argc, argv, "c")) != -1)
	{
		switch(c)
		{
		case 'c':
			sscanf(optarg, "%i", &wrpc_base);
			break;
		default:
			fprintf(stderr,
				"Use: \"%s "
				"[-c wrpc base address] <lm32_program.bin>\"\n",
				argv[0]);
			fprintf(stderr,
				"By default, the WRPC is assumed at 0x%x.\n",
				wrpc_base);
			exit(1);
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "%s: Expected binary name after options.\n",
			argv[0]);
		exit(1);
	}

	printf("LM32 load starting..\n");
	if(fasec_load_lm32(argv[optind], wrpc_base) < 0)
	{
		fprintf(stderr, "%s: Loader failure.\n", argv[0]);
		exit(1);
	}

	printf("loading finished!\n\n");
	exit (0);
}

