/******************************************************************************
*
* Copyright (C) CERN, 2016
* Pieter Van Trappen
* CERN TE-ABT-EC
* Licence: GPL v2 or later
*
* modified bare-bone header-file for use in embedded linux
******************************************************************************/

#ifndef I2C_LIB_H_
#define I2C_LIB_H_

unsigned i2c_readReg(void *base, unsigned addr);
void i2c_writeReg(void *base, unsigned addr, unsigned val);
int waitTransfer(void *base);
int waitAck(void *base);
int OCI2C_init(void *base);
void OCI2C_print_regs(void *base, unsigned start, unsigned end);
unsigned readReg_24AA64(void *base, unsigned slave, unsigned readAddrH, unsigned readAddrL);
unsigned readReg_mcp23017(void *base, unsigned slave, unsigned readAddr);
unsigned readReg_88e1111(void *base, int slave, int readAddr);
int writeReg_mcp23017(void *base, unsigned slave, unsigned writeAddr, unsigned val);

#endif /* I2C_LIB_H_ */
