/*
 * Placeholder PetaLinux user application.
 *
 * Replace this with your application code
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include "i2c_lib.h"

#define OCI2CBASEFMC	0x43c00000		// I2C core for FMC1-2 I2C bus
#define OCI2CBASEFE	0x43c10000		// I2C core for FASEC EEPROM
#define FHWTEST		0x43c20000		// FASEC HW-TEST core base
#define OCI2CSFP	0x43c30000		// I2C core for SFP PHY

#define OCI2CMEM 0x10000

int main(int argc, char *argv[])
{
	const int MAXUIO = 3;
	const int NOARGS = 6; //number of arguments after executable
	int fd, i, maxread, minread;
	int args[NOARGS];
	void *uiop;
	int uchoice;
	char *uio = "/dev/uio";
	char str[80];
	unsigned data;

	printf("Driver for an I2C device. Usage: (all args decimal integers!)\n");
	printf("%s /dev/uio slave 12/8-bit-address 16/8-bit-data #startreg #endreg-to-print\n", argv[0]);
	if(argc!=NOARGS+1){
		printf("Invalid number of arguments, exiting\n");
		exit(-1);
	} else{
		for(i=1;i<argc;i++)
			args[i-1] = atoi(argv[i]);
	}

	strcpy(str, uio);
	uchoice = args[0];
	if(uchoice>MAXUIO){
		printf("Invalid argument for /dev/uio. \n");
		exit(-1);
	}
	strcat(str, argv[1]);

	// open User I/O device (see DTS)
	//TODO: open /sys/class/uio/uio./maps/map0/addr to print address
	printf("Opening device %s\n",str);
	fd = open(str, O_RDWR);
	if (fd<1){
		fprintf(stderr,"Invalid UIO device file.\n");
		exit(-1);
	}
	// mmap the device
	uiop = mmap(NULL, OCI2CMEM, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if((int)uiop==-1){
		fprintf(stderr,"Error mmapping the UIO memory");
		exit(-1);
	}

	// interact with I2C device
	minread = args[4];
	maxread = args[5];
	if(!((minread>-1) && (minread<256)))
		minread = 0;
	if(!((maxread>0) && (maxread<256)))
		maxread = 255;
	OCI2C_init(uiop);
	OCI2C_print_regs(uiop,0,1);
	for(i=minread;i<maxread;i++){
		if(args[2]==12)
			data = readReg_24AA64(uiop, (unsigned) args[1], 0, (unsigned) i);
		else if(args[2]==8 && args[3]==8)
			data = readReg_mcp23017(uiop, (unsigned) args[1], (unsigned) i);
		else if (args[2]==8 && args[3]==16)
			data = readReg_88e1111(uiop, args[1], i);
		else
			exit(-1);
		printf("reg %3d of slave %3d: 0x%04x\n", i, args[1], data);
	}

	// unmap the address range
	munmap(uiop, OCI2CMEM);

	return 0;
}


