/******************************************************************************
*
* Copyright (C) CERN, 2016
* Pieter Van Trappen
* CERN TE-ABT-EC
* Licence: GPL v2 or later
*
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // for sleep

// I2C OC registers
#define PREL	0x0		// clock prescale low
#define PREH	0x1		// clock prescale high
#define CTR		0x2		// control register
#define TXR		0x3		// transmit register (W)
#define RXR		0x3		// receive register (R)
#define CR		0x4		// command register (W)
#define SR		0x4		// status register (R)
// masks
#define CTR_EN 7
#define CR_STA 7
#define CR_STO 6
#define CR_RD 5
#define CR_WR 4
#define CR_ACK 3
#define SR_RXACK 7
#define SR_TIP 1
// misc
#define TIMEOUT 2000	// high timeout needed because of I2C clock = 100 kHz

unsigned i2c_readReg(void *base, unsigned addr){
	return *((unsigned *)(base + (addr<<2)));
}
void i2c_writeReg(void *base, unsigned addr, unsigned val){
	*((unsigned *)(base + (addr<<2))) = val;
}

int waitTransfer(void *base){
	int i;
	for(i=TIMEOUT;i!=0;i--){
		if ((i2c_readReg(base, SR) & 1<<SR_TIP) == 0x0){
//			usleep(100);	// sleep 100 us otherwise spurious 'no RxACK' -- no influence
			return 0;
		}
	}
	printf("ERROR: transfer not completed\n\r");
	i2c_writeReg(base, CR, 1<<CR_STO);	// stop signal to abort data transfer
	return (i==0)?1:0;	// return 0 when success
}

int waitAck(void *base){
	if ((i2c_readReg(base, SR) & 1<<SR_RXACK) != 0x0){
		printf("ERROR: no byte RxACK from slave seen!\n\r");
		i2c_writeReg(base, CR, 1<<CR_STO);	// stop signal to abort data transfer
		return 2;
	} else{
		return 0;
	}
}
int OCI2C_init(void *base){
	// inits for 100 kHz operation
	if ((i2c_readReg(base, CTR) & 1<<CTR_EN) == 1<<CTR_EN)
		i2c_writeReg(base, CTR, 0<<CTR_EN);	// disable EN bit so we can set the prescale value
	i2c_writeReg(base, PREL, 0xC8);	// 100MHz/(5*0.1MHz)
	i2c_writeReg(base, PREH, 0x00);
	printf("prescale value set \n\r");
	i2c_writeReg(base, CTR, 1<<CTR_EN);	// enable core without inter\rupts
	return 0;
}

void OCI2C_print_regs(void *base, unsigned start, unsigned end){
	int i;
	printf("** OCI2C register print starting **\n\r");
	for (i=start; i<end+1;i++){
		printf("I2C master register %#010x: %#010x \n\r", (unsigned int)base + (i<<2),
				(unsigned int)i2c_readReg(base, i));
	}
	printf("OCI2C register print finished \n\r");
}

unsigned readReg_24AA64(void *base, unsigned slave, unsigned readAddrH, unsigned readAddrL){
	/* for the 24AA64 EEPROM chip
	 * 12-bit address so 2 bytes are sent for the address
	 */
	// send control byte (write)
	i2c_writeReg(base, TXR, slave<<1 | 0x0);	// address & write-bit
	i2c_writeReg(base, CR, 1<<CR_STA | 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
	// send read-addresses high
//		printf("sending read address high\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, TXR, readAddrH);
	i2c_writeReg(base, CR, 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
	// send read-addresses low
//		printf("sending read address low\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, TXR, readAddrL);
	i2c_writeReg(base, CR, 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
	// send control byte (read)
//		printf("sending control byte for reading\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, TXR, slave<<1 | 0x1);	// address & read-bit
	i2c_writeReg(base, CR, 1<<CR_STA | 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
	// send CR byte to prepare read
//		printf("reading ...\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, CR, 1<<CR_RD | 1<<CR_ACK | 1<<CR_STO);
	if (waitTransfer(base)!=0)
		return 1;
	// read output
	return i2c_readReg(base, RXR);
}

unsigned readReg_mcp23017(void *base, unsigned slave, unsigned readAddr){
	/* 
	 * 1-byte addressing and data
	 */
//		printf("sending control byte - write\n\r");
	i2c_writeReg(base, TXR, slave<<1 | 0x0);	// address & write-bit
	i2c_writeReg(base, CR, 1<<CR_STA | 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
//		printf("sending read address\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, TXR, readAddr);
	i2c_writeReg(base, CR, 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
//		printf("sending control byte for reading\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, TXR, slave<<1 | 0x1);	// address & read-bit
	i2c_writeReg(base, CR, 1<<CR_STA | 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
//		printf("reading ...\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, CR, 1<<CR_RD | 1<<CR_ACK | 1<<CR_STO);
	if (waitTransfer(base)!=0)
		return 1;
	// read output
	return i2c_readReg(base, RXR);
}

unsigned readReg_88e1111(void *base, int slave, int readAddr){
	/* 
	 * 1-byte addressing but 2-byte data reply
	 * no datasheet (confidential), trying with ack & stop-bit 0
	 * worked only once (1st 2-bytes), trying repeated start
	 */
//		printf("sending control byte - write\n\r");
	unsigned b0, b1;
	i2c_writeReg(base, TXR, slave<<1 | 0x0);	// address & write-bit
	i2c_writeReg(base, CR, 1<<CR_STA | 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
//		printf("sending read address\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, TXR, readAddr);
	i2c_writeReg(base, CR, 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
//		printf("sending control byte for reading\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, TXR, slave<<1 | 0x1);	// address & read-bit
	i2c_writeReg(base, CR, 1<<CR_STA | 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
//		printf("reading ...\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, CR, 1<<CR_RD | 1<<CR_ACK | 0<<CR_STO); // ack but no stop bit
	if (waitTransfer(base)!=0)
		return 1;
	// read output byte 0
	b0 = i2c_readReg(base, RXR);
	i2c_writeReg(base, TXR, slave<<1 | 0x1);	// address & read-bit
	i2c_writeReg(base, CR, 1<<CR_STA | 1<<CR_WR);   // repeated start
	if (waitTransfer(base)!=0)
		return 1;
//		printf("reading ...\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, CR, 1<<CR_RD | 1<<CR_ACK | 1<<CR_STO); // ack and stop
	if (waitTransfer(base)!=0)
		return 1;
	// read output byte 1
	b1 = i2c_readReg(base, RXR);
	return (b1<<8)+b0;
}

int writeReg_mcp23017(void *base, unsigned slave, unsigned writeAddr, unsigned val){
	/*
	 * IOCON all 0 by default, hence:
	 *  same bank reg - sequential regs
	 *  address pointer increments
	 */
//		printf("sending control byte - write\n\r");
	i2c_writeReg(base, TXR, slave<<1 | 0x0);	// address & write-bit
	i2c_writeReg(base, CR, 1<<CR_STA | 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
//		printf("sending write address\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, TXR, writeAddr);
	i2c_writeReg(base, CR, 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
//		printf("sending control byte for writing\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base, TXR, slave<<1 | 0x0);	// address & write-bit
	i2c_writeReg(base, CR, 1<<CR_STA | 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
//		printf("sending data byte for write ...\n\r");
	if (waitAck(base)!=0)
		return 2;
	i2c_writeReg(base,TXR,val);
	i2c_writeReg(base, CR, 1<<CR_STO | 1<<CR_WR);
	if (waitTransfer(base)!=0)
		return 1;
	if (waitAck(base)!=0)
		return 2;
	// finished
	return 0;
}
