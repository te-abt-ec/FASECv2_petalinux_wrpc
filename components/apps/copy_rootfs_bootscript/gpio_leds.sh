#!/bin/bash
# turn on/off a certain amount of IO from a gpiochip
# usage: ./gpio_leds.sh gpiochip amount 0/1

readonly MAXPINS=16

if [ ! -d "/sys/class/gpio/gpiochip"$1 ]; then
    echo "gpiochip not found!"
    exit 1
fi
if [ $2 -gt $MAXPINS ]; then
    echo "too many IOs given!"
    exit 1
fi

for i in $(seq 0 $(($2-1)))
do
    gpio=$(($1+i))
    # exporting first cause possible new IC (not configured) connected
    if [ -d "/sys/class/gpio/gpio"$gpio ]; then
	echo $gpio > /sys/class/gpio/unexport
    fi
    echo $gpio > /sys/class/gpio/export
    echo out > "/sys/class/gpio/gpio"$gpio"/direction"
    echo $3 > "/sys/class/gpio/gpio"$gpio"/value"
done
