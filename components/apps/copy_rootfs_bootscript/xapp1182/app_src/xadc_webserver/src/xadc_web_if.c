/*****************************************************************************
 *
 *     Author: Xilinx, Inc. (c) Copyright 2012 Xilinx Inc.
 *     All rights reserved.
 *
 *     This file may be used under the terms of the GNU General Public
 *     License version 3.0 as published by the Free Software Foundation
 *     and appearing in the file LICENSE.GPL included in the packaging of
 *     this file.  Please review the following information to ensure the
 *     GNU General Public License version 3.0 requirements will be met.
 *     http://www.gnu.org/copyleft/gpl.html.
 *
 *     With respect to any license that requires Xilinx to make available to
 *     recipients of object code distributed by Xilinx pursuant to such
 *     license the corresponding source code, and if you desire to receive
 *     such source code from Xilinx and cannot access the internet to obtain
 *     a copy thereof, then Xilinx hereby offers (which offer is valid for as
 *     long as required by the applicable license; and we may charge you the
 *     cost thereof unless prohibited by the license) to provide you with a
 *     copy of such source code; and to accept such offer send a letter
 *     requesting such source code (please be specific by identifying the
 *     particular Xilinx Software you are inquiring about (name and version
 *     number), to:  Xilinx, Inc., Legal Department, Attention: Software
 *     Compliance Officer, 2100 Logic Drive, San Jose, CA U.S.A. 95124.
 *
 *     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
 *     AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND
 *     SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,
 *     OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,
 *     APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION
 *     THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
 *     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
 *     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
 *     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
 *     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
 *     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
 *     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *     FOR A PARTICULAR PURPOSE.
 *
 *     CRITICAL APPLICATIONS WARRANTIES
 *     Xilinx products are not designed or intended to be fail-safe, or
 *     for use in any application requiring fail-safe performance, such as
 *     life-support or safety devices or systems, Class III medical devices,
 *     nuclear facilities, applications related to the deployment of airbags,
 *     or any other applications that could lead to death, personal injury,
 *     or severe property or environmental damage (individually and
 *     collectively,  "Critical Applications"). Customer assumes the sole
 *     risk and liability  of any use of Xilinx products in Critical
 *     Applications, subject only to applicable laws and regulations
 *     governing limitations on product liability.
 *
 *     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF
 *     THIS FILE AT ALL TIMES.
 *
 *     This file is a part of sobel_qt application, which is based in part
 *     on the work of the Qwt project (http://qwt.sf.net).
 *
 *****************************************************************************/


#include <stdio.h>
#include <unistd.h>
#include <string.h>

// modif pvt for header file location (see makefile)
#include "xadc_core_if.h"

#define HTTP_PORT    9090

int webserver_init(int port);

int get_XadcStats(char *buffer, unsigned int len)
{

	xadc_update_stat();	// must to update the cache

	memset(buffer,0,len);
	sprintf(buffer, "%.2f,%.2f,%.2f,%.2f,%.2f,%d,%d,%d,%d,",
			xadc_get_value(EParamVccInt),
			xadc_get_value(EParamVccAux),
			xadc_get_value(EParamVccBRam),
			xadc_get_value(EParamTemp),
			xadc_get_value(EParamVAux0),
			xadc_get_alarm_status(EAlarmVccInt_TH),
			xadc_get_alarm_status(EAlarmVccAux_TH),
			xadc_get_alarm_status(EAlarmVccBRam_TH),
			xadc_get_alarm_status(EAlarmTemp_TH)    );

	return 0;
}

int set_Param(char *buf)
{
	float min[EAlarmMAX];
	float max[EAlarmMAX];
	static float prev_min[EAlarmMAX] = {-1};
	static float prev_max[EAlarmMAX]= {-1};

	int i;
	sscanf(buf, "%f,%f,%f,%f,%f,%f,%f,%f",
			&min[EAlarmVccInt_TH],
			&max[EAlarmVccInt_TH],
			&min[EAlarmVccAux_TH],
			&max[EAlarmVccAux_TH],
			&min[EAlarmVccBRam_TH],
			&max[EAlarmVccBRam_TH],
			&min[EAlarmTemp_TH],
			&max[EAlarmTemp_TH]
			     );

	// Write the threshold value
	for(i =0 ; i< EAlarmMAX; i++)
	{
		if(prev_min[i] != min[i] || prev_max[i] != max[i])
		{
			xadc_set_threshold(i,min[i],max[i],NULL);
			prev_min[i] = min[i];
			prev_max[i] = max[i];
		}

	}
	return 0;
}

int get_CmdExec(char *cmd, char *result, int size)
{
	FILE *in;
	char *p, buf[4*1024];

	memset(result, 0, size);
	in = popen(cmd, "r");
	if(in == NULL)
		return -1;

	while(1)
	{
		p = fgets(buf, sizeof(buf), in);
		if(p == NULL)
			break;
		strcat(result, buf);
	}
	pclose(in);
	return 0;
}

int main()
{
	int ret = 0;

	if((ret = (xadc_core_init(EXADC_INIT_FULL))) == 0)
		webserver_init(HTTP_PORT);

	xadc_core_deinit();

	return ret;
}

