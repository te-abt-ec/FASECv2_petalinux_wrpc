/*****************************************************************************
 *
 *     Author: Xilinx, Inc. (c) Copyright 2012 Xilinx Inc.
 *     All rights reserved.
 *
 *     This file may be used under the terms of the GNU General Public
 *     License version 3.0 as published by the Free Software Foundation
 *     and appearing in the file LICENSE.GPL included in the packaging of
 *     this file.  Please review the following information to ensure the
 *     GNU General Public License version 3.0 requirements will be met.
 *     http://www.gnu.org/copyleft/gpl.html.
 *
 *     With respect to any license that requires Xilinx to make available to
 *     recipients of object code distributed by Xilinx pursuant to such
 *     license the corresponding source code, and if you desire to receive
 *     such source code from Xilinx and cannot access the internet to obtain
 *     a copy thereof, then Xilinx hereby offers (which offer is valid for as
 *     long as required by the applicable license; and we may charge you the
 *     cost thereof unless prohibited by the license) to provide you with a
 *     copy of such source code; and to accept such offer send a letter
 *     requesting such source code (please be specific by identifying the
 *     particular Xilinx Software you are inquiring about (name and version
 *     number), to:  Xilinx, Inc., Legal Department, Attention: Software
 *     Compliance Officer, 2100 Logic Drive, San Jose, CA U.S.A. 95124.
 *
 *     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
 *     AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND
 *     SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,
 *     OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,
 *     APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION
 *     THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
 *     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
 *     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
 *     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
 *     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
 *     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
 *     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *     FOR A PARTICULAR PURPOSE.
 *
 *     CRITICAL APPLICATIONS WARRANTIES
 *     Xilinx products are not designed or intended to be fail-safe, or
 *     for use in any application requiring fail-safe performance, such as
 *     life-support or safety devices or systems, Class III medical devices,
 *     nuclear facilities, applications related to the deployment of airbags,
 *     or any other applications that could lead to death, personal injury,
 *     or severe property or environmental damage (individually and
 *     collectively,  "Critical Applications"). Customer assumes the sole
 *     risk and liability  of any use of Xilinx products in Critical
 *     Applications, subject only to applicable laws and regulations
 *     governing limitations on product liability.
 *
 *     THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF
 *     THIS FILE AT ALL TIMES.
 *
 *     This file is a part of sobel_qt application, which is based in part
 *     on the work of the Qwt project (http://qwt.sf.net).
 *
 *****************************************************************************/

/*****************************************************************************
 *
 * @file zc706_psxadc_webserver.c
 *
 * Implementation of Webserver application for zc706 PS XADC Appnote
 *
 ******************************************************************************/

/*=====================================*/
/*  INCLUDES                           */
/*=====================================*/
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>

/*=====================================*/
/*  MACROS and DEFINITIONS             */
/*=====================================*/
#undef	MAIN_APP
#undef  DEBUG
//#define DEBUG_MODE 1

//utilities
#ifdef DEBUG_MODE
	#define DEBUG_Text(fmt) printf(fmt)
	#define DEBUG_Printf(fmt,...) printf(fmt,__VA_ARGS__)
#else
	#define DEBUG_Text(fmt)
	#define DEBUG_Printf(fmt,...)
#endif

// -- macros for webserver interface
#define SERVER            "Server: Dynamic Thread\n"
#define CONTENT           "Content-Type: text/html\r\n"
#define	WEBSERVER_FOLDER  "Webserver/"

/*=====================================*/
/*  GLOBAL VARIABLES                   */
/*=====================================*/
int TcpPort = 9090;	// default
int listenSock = 0;
struct sockaddr_in server;
pthread_t thread;
pthread_t service_thread;

/*=====================================*/
/*  FUNCTION PROTOTYPES                */
/*=====================================*/
extern int get_XadcStats(char *buffer, unsigned int len);
extern int set_Param(char *buf);
extern int get_CmdExec(char *cmd, char *result, int size);

int webserver_init(int port);
void *webservice(void *arg);
void *parseHttpReq(void *arg); //Parse the HTTP request
void badRequest(int sock);  //Send HTTP header for bad request (400)
void serve_get(int sock, char *path);  //Serve the file
void serve_post(int sock, char *path);
void goodRequest(int sock, char* contentType, int size);  //Send HTTP header for good request (200)
void notGet(int sock); //Send HTTP header for unimplemented method (501)
void notFound(int sock); //Send HTTP header when a file is not found (404)

/*------------------------------------------------------------
 * main() - Entry function to the application
 *          This function takes maximum of one commandline
 *          argument (webserver TCP port number)
 *          If the argument is not passed, default port is 9090
 *
 * @argc - command line argument count
 * @argv - command line arguments
 *
 * return: SUCCESS or FAILURE
 *------------------------------------------------------------*/
#ifdef MAIN_APP
int main(int argc, char *argv[])
#else
int webserver_init(int port)
#endif
{
    int result = 0;
//    int val = 1;

    #ifdef MAIN_APP
    //Determine of port number was input
    if(argc != 2)
        fprintf(stderr, "Port number not input, default=%d !\n", TcpPort);
    else
    {
        //Get port number
        TcpPort = atoi(argv[1]);
        DEBUG_Printf("port number: %d\n", TcpPort);
    }
    #else
      if(port < 0)
        TcpPort = 9090;	// default TCP port
      else
    TcpPort = port;
    #endif

    /*----------------------------------*/
    /*  Start Webserver                 */
    /*  Listen Clients continuously     */
    /*----------------------------------*/
    result = pthread_create(&service_thread, NULL, webservice, NULL);
    if(result != 0)
    {
        fprintf(stderr, "Could not create thread!\n");
        close(listenSock);
	exit(1);
    }

    /*----------------------------------*/
    /*  wait till the threads are done  */
    /*----------------------------------*/

    pthread_join(service_thread, NULL);
    return 0;
}

/*------------------------------------------------------------
 * webservice() - this thread starts the webservice,
 *          start listen to client requests, serve the
 *          requests. This thread runs infinitely, till app is
 *          exit (press Ctrl+C)
 *
 * @argc - TCP port number of to start the Webservice
 *
 * return: SUCCESS or FAILURE
 *------------------------------------------------------------*/
void *webservice(void *arg)
{
    int result;
    int val;
    int port = TcpPort;

    /* -------- Create socket -------- */
    DEBUG_Text("open socket...");
    listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(listenSock == -1)
    {
        fprintf(stderr, "Could not creat a socket!\n");
        return (void*)-1;
    }
    DEBUG_Text("done\n");

    /* -------- Set socket option -------- */
    DEBUG_Text("set socket opt...");
    result = setsockopt(listenSock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
    DEBUG_Text("done\n");

    /* -------- Set up address structure -------- */
    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(port);

    /* -------- Bind socket to port -------- */
    DEBUG_Text("bind...");
    result = bind(listenSock, (struct sockaddr*)&server, sizeof(server));
    if(result != 0)
    {
        fprintf(stderr, "Could not bind to port!\n");
        close(listenSock);
        return (void*)-1;
    }
    DEBUG_Text("done\n");

    /* -------- Listen for client connections -------- */
    DEBUG_Text("listen...");
    result = listen(listenSock, 5);
    if(result == -1)
    {
        fprintf(stderr, "Cannot listen on socket!\n");
        close(listenSock);
        return (void*)-1;
    }
    DEBUG_Text("done\n");

    /*-----------------------------------------------*/
    /* This is an infinite loop, executing below     */
    /* steps continuosly                             */
    /*   - wait for a request from client            */
    /*   - read the request                          */
    /*   - process the request                       */
    /*   - send response to client                   */
    /*   - repeat the above steps                    */
    /* This loop is broken by entering Ctrl+C        */
    /*-----------------------------------------------*/
    while(1)
    {
        struct sockaddr_in client = {0};
        int newSock = 0;
        unsigned int clientL = sizeof(client);

        /* -------- accept connections -------- */
        DEBUG_Text("accept...\n");
        newSock = accept(listenSock, (struct sockaddr*)&client, &clientL);
        if(newSock == -1)
        {
            DEBUG_Printf( "Cannot accept connection! newSock=%d, errno=%d\n", newSock, errno );
            sleep(2);
            continue;
	    //close(listenSock);
	    //exit(1);
        }
        else
        {
            /* -------- Create a thread for each incoming connection -------- */
	    result = pthread_create(&thread, NULL, parseHttpReq, (void*) newSock);
	    if(result != 0)
	    {
	    	DEBUG_Text("Could not create thread!\n");
	        close(newSock);
                sleep(2);
                continue;
	        //exit(1);
            }

            /* -------- wait till the client request is serviced -------- */
            pthread_join(thread, NULL);
            close(newSock);
            sched_yield();

        } //End of else -- could accept
    } //End of while

    close(listenSock);
    return 0;

}//End of main

/*------------------------------------------------------------
 * parseHttpReq() - function to handle the client request
 *          this function parses the client request, perform
 *          necessary action and send the results back to client
 *          over the HTTP
 *
 * @argc - TCP socket number of this request
 *
 * return: SUCCESS or FAILURE
 *------------------------------------------------------------*/
void *parseHttpReq(void *arg)
{
    int sock;
    char buffer[1024*20];
    int readIn;
    char *method, *path, *version;
    int v1, v2, m;

    DEBUG_Text("parseHttpReq\n");
    //cast sock back to int
    sock = (int)arg;

    /* -------- Get input from client -------- */
    DEBUG_Text("recv Http packet\n");
    readIn = recv(sock, buffer, sizeof(buffer)-10, 0);
    buffer[readIn] = '\0';
    //printf("recv pkt size %d: \n================\n%s\n================\n", readIn, buffer);

    /*---------------------------------------*/
    /* check if it is valid request          */
    /* Parse the request string for a valid  */
    /* command and associated parameters     */
    /*---------------------------------------*/
    if(readIn > 0)
    {
        /* -------- parse for  HTTP method, file path, HTTP version -------- */
        method = (char*)malloc(sizeof(buffer)+1);
        strcpy(method, buffer);  //copy header data into method
        method = strtok(method, " ");
        path = strtok(NULL, " ");
        version = strtok(NULL, "\r\n");

        /* -------- validate HTTP version -------- */
        v1 = strcmp(version, "HTTP/1.0\r\n");
        v2 = strcmp(version, "HTTP/1.1\r\n");
        if(!v1)
            strcpy(version, "HTTP/1.0");
        else
            strcpy(version, "HTTP/1.1");

        DEBUG_Printf("method : %s\n", method);
        DEBUG_Printf("path   : %s\n", path);
        DEBUG_Printf("version: %s\n", version);

        /* -------- ERROR: version mismatch, send 400 error message -------- */
        if((!v1) || (!v2))
        {
            badRequest(sock);
            return 0;
        }

        /* -------- See if the method is "GET" -------- */
        m = strcmp(method, "GET");
        if(m == 0)
	{
            serve_get(sock, path);
	}
        else
	{
            /* -------- See if the method is "POST" -------- */
            m = strcmp(method, "POST");
            if(m == 0)
            {
                serve_post(sock, path);
            }
            else
            {
                /* -------- Unsupported method -------- */
                notGet(sock);
            }
        }
    }

    DEBUG_Text("End of parseHttpReq\n");
    return 0;

} //End of parseHttpReq

/*------------------------------------------------------------
 * notGet() - function to handle unsupported method from client
 *
 * @argc - TCP socket number of this request
 *
 * return: SUCCESS or FAILURE
 *------------------------------------------------------------*/
void notGet(int sock)
{
    char buffer[1024];

    /* -------- Send HTTP Response line by line -------- */
    DEBUG_Text("======== unsupported method ========\n");
    strcpy(buffer, "HTTP/1.0 501 Method Not Implemented\r\n");
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, SERVER);
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, CONTENT);
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, "\r\n");
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, "<html>\n<head>\n<title>Method Not Implemented</title>\n</head>\r\n");
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, "<body>\n<p>501 HTTP request method not supported.</p>\n</body>\n</html>\r\n");
    write(sock, buffer, strlen(buffer));

} //End end of notGet

/*------------------------------------------------------------
 * badRequest() - function to handle bad HTTP request from client
 *
 * @argc - TCP socket number of this request
 *
 * return: SUCCESS or FAILURE
 *------------------------------------------------------------*/
void badRequest(int sock)
{
    char buffer[1024];

    /* -------- Send HTTP Response line by line -------- */
    DEBUG_Text("======== bad request ========\n");
    strcpy(buffer, "HTTP/1.0 400 Bad Request\r\n");
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, SERVER);
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, CONTENT);
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, "\r\n");
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, "<html>\n<head>\n<title>Bad Request</title>\n</head>\r\n");
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, "<body>\n<p>400 Bad Request.</p>\n</body>\n</html>\r\n");
    write(sock, buffer, strlen(buffer));

} //End of badRequest

/*------------------------------------------------------------
 * notFound() - function to handle failed to serve request
 *
 * @argc - TCP socket number of this request
 *
 * return: SUCCESS or FAILURE
 *------------------------------------------------------------*/
void notFound(int sock)
{
    char buffer[1024];

    /* -------- Send HTTP Response line by line -------- */
    DEBUG_Text("======== not found ======== \n");
    strcpy(buffer, "HTTP/1.0 404 Not Found\r\n");
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, SERVER);
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, CONTENT);
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, "\r\n");
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, "<html>\n<head>\n<title>Not Found</title>\n</head>\r\n");
    write(sock, buffer, strlen(buffer));
    strcpy(buffer, "<body>\n<p>404 Request file not found.</p>\n</body>\n</html>\r\n");
    write(sock, buffer, strlen(buffer));

} //End of notFound

/*------------------------------------------------------------
 * goodRequest() - function to handle good requst from client
 *                 after serving the request, HERE the result
 *                 is sent to client
 *
 * @argc - TCP socket number of this request
 *
 * return: SUCCESS or FAILURE
 *------------------------------------------------------------*/
void goodRequest(int sock, char* contentType, int size)
{
    char buffer[1024];

    /* -------- Send HTTP Response line by line -------- */
    DEBUG_Printf("goodRequest: %s, size=%d \n", contentType, size);

    // header
    strcpy(buffer, "HTTP/1.0 200 Ok\r\n");
    send(sock, buffer, strlen(buffer), 0);
    // content
    strcpy(buffer, contentType);
    send(sock, buffer, strlen(buffer), 0);
    // length
    sprintf(buffer, "Content-length: %d", size);
    send(sock, buffer, strlen(buffer), 0);
    // connection: close
    strcpy(buffer, "Connection: close\r\n");
    send(sock, buffer, strlen(buffer), 0);
    // end of header
    strcpy(buffer, "\r\n");
    send(sock, buffer, strlen(buffer), 0);

} //End of good request

/*------------------------------------------------------------
 * serve_get() - function to handle GET request from client
 *
 * @argc - TCP socket number of this request
 *
 * return: SUCCESS or FAILURE
 *------------------------------------------------------------*/
void serve_get(int sock, char *path)
{
    char* file;
    int fd;
    char buffer[1024 * 1024];  //Only supports files <= 1MB
    char file_path[2048];
    int size;
    char *fext;

    DEBUG_Text("serve GET request\n");

    /* -------- determine the filename requested -------- */
    if(strlen(path) == 1)
    {
        strcpy(path, "index.html");
        file = path;
    }
    else
        file = path+1;	// skip the leading "/" character

    /* -------- Get file extension -------- */
    fext = file+strlen(file-1);
    while (fext > file)
    {
        if (*fext == '.')
        {
            fext += 1;
	    break;
        }
        fext--;
    }
    DEBUG_Printf("File extension: %s\n", fext);
    memset(file_path, 0, sizeof(file_path) );
    strcpy(file_path, WEBSERVER_FOLDER);
    strcat(file_path, file);

    /* -------- Open file -------- */
    DEBUG_Printf("opening file: %s\n", file);
    fd = open(file_path, O_RDONLY);
    if(fd == -1)
    {
        notFound(sock);
        return;
    }

    /* -------- Read in file and store in buffer -------- */
    size=read(fd, buffer, sizeof(buffer));
    close(fd);

    /* -------- Prepare Http response header based on file type -------- */
    if( !strncmp(fext, "htm", 3))
        goodRequest(sock, "Content-Type: text/html\r\n", size);
    else if( !strncmp(fext, "JPG", 3))
        goodRequest(sock, "Content-Type: image/jpeg\r\n", size);
    else if( !strncmp(fext, "jif", 3))
        goodRequest(sock, "Content-Type: image/jif\r\n", size);
    else if( !strncmp(fext, "css", 3))
        goodRequest(sock, "Content-Type: text/css\r\n", size);
    else if( !strncmp(fext, "svg", 3))
        goodRequest(sock, "Content-Type: image/svg+xml\r\n", size);
    else
        goodRequest(sock, "Content-Type: text/html\r\n", size);

    /* -------- Send response to client -------- */
    DEBUG_Printf("== sending %s, size=%d bytes\n", file, size);
    send(sock, buffer, size, 0);

    return;
}

/*------------------------------------------------------------
 * serve_post() - function to handle POST request from client
 *
 * @argc - TCP socket number of this request
 *
 * return: SUCCESS or FAILURE
 *------------------------------------------------------------*/
int pattern=0;
void serve_post(int sock, char *path)
{
    char* file;
 //   char *fext;
    char buf[32*1024];
    int size;
    //static int reqNo=0;

    DEBUG_Text("serve POST request\n");
   DEBUG_Printf("\n\nreqCount: %d\n", reqNo++);

    /* -------- determine the filename requested -------- */
    file = path+5;	// skip the leading "/"
    DEBUG_Printf("file: %s\n", file);

    /*--------------------------------------------------------*/
    /* These requests from client are specific to application */
    /* Invoke Application hook functions as per the request   */
    /*--------------------------------------------------------*/
    memset(buf, 0, sizeof(buf) );

    /* -------- request to application: STATS -------- */
    if( !strncmp(file,"stats", 5) )
    {
    	DEBUG_Text("======== GET STATISTICS \n");
        pattern= atoi(path+11);
        get_XadcStats(buf, sizeof(buf) );
	DEBUG_Text(buf);
	
    }

    /* -------- request to application: SETUP -------- */
    if( !strncmp(file,"setup", 5) )
    {
    	DEBUG_Text("======== SETUP PARAMETERS \n");
        set_Param( (char*)(path+11) );
        strcpy(buf, "ok");
    }

    /* -------- request to application: SHCMD -------- */
    if( !strncmp(file,"shcmd", 5) )
    {
	    fprintf(stderr,"schell-commands deactivated\n");

        /*-----------------------------------------------------*/
        /* the command string from client may contain special  */
        /* characters. These characters need to be handled     */
        /* properly, else the command DOES NOT get executed    */
        /* Below are the checks                                */
        /*-----------------------------------------------------*/

/*	DEBUG_Text("======== LINUX COMMAND \n");
        int i, j, k;
	char cmd[2*1024];
	i=11; j=0;
        while( *(path+i) != '\0' )
        {
	    cmd[j] = path[i];
            k=1;

            // check1: replace %20 with space
            if( (path[i]== '%') && (path[i+1] == '2') && (path[i+2] == '0') )
            {
                cmd[j] = ' ';
                k=3;
	    }

            // check2: replace %2F with '/'
	    if( (path[i]== '%') && (path[i+1] == '2') && (path[i+2] == 'F') )
	    {
	        cmd[j] = '/';
	        k=3;
	    }
	    i += k;
	    j++;
        }

        DEBUG_Printf("Linux command to execute: %s\n", (char*)(cmd) );
        get_CmdExec( (char*)(cmd), buf, sizeof(buf) );
*/
    }

    /* -------- Send response from App to client -------- */
    size = strlen(buf);
    goodRequest(sock, "Content-Type: text/html\r\n", size );
    send(sock, buf, size, 0);

} //End of serve_post

