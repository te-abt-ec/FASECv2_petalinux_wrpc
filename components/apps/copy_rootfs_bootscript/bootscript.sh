#!/bin/bash

# set the time (ntpc not installed)
rdate -s time-c.nist.gov

# required for the xadc webserver app
ln -s /lib/ld-2.19-2014.08.so /lib/ld-linux.so.3

# load the fasec_hwtest module (assuming patch panel is connected!)
modprobe -v fasec_fids.ko

